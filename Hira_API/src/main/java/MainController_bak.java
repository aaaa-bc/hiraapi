/*
 * 2017-11-20 Made by YJ
 * https://www.data.go.kr/dataset/15001697/openapi.do 병원코드정보 서비스 
 * */

import org.json.JSONObject; 

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.simple.parser.JSONParser;

import com.mashape.unirest.http.HttpResponse; 
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;


public class MainController_bak {

	
	public static void main(String[] args) throws Exception {
		
		//1. JSON 리퀘스트 
		JSONObject rawobj = Request_JSON();
		
		//2. 파싱
		JSONArray itemlist = rawobj.getJSONObject("response").getJSONObject("body").getJSONObject("items").getJSONArray("item");
		
		
		System.out.println(itemlist.toString());
		
		//3. 뽑아서 출력
		List<String> result = new ArrayList<String>();
		for(int i =0; i<itemlist.length();i++){
			String temp[] = new String[2];
			temp[0] = (String) itemlist.getJSONObject(i).get("clCd").toString();
			temp[1] = (String) itemlist.getJSONObject(i).get("clCdNm");
			result.add(temp[0]+'%'+temp[1]);
		}
		
		System.out.println(result.toString());
		
	
		
	}
	
    public static JSONObject Request_JSON() throws Exception {    	
    	 String url = "https://api.bithumb.com/public/ticker/btc";
         String url2 = "http://apis.data.go.kr/B551182/codeInfoService/getMedicInsttClassesCodeList?serviceKey="+Config.APIKEY+"&pageNo=1&numOfRows=100&_type=json";
    	 HttpResponse<JsonNode> response = Unirest.get(url2).asJson();
        return response.getBody().getObject();
    }
}