import org.json.JSONObject;

import static org.hamcrest.CoreMatchers.instanceOf;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.hamcrest.core.IsNull;
import org.json.JSONArray;
import org.json.simple.parser.JSONParser;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class DetailController extends MainController {

	public static List<String> ykihoList = insertController.get_YkihoList();	
	
	
	//start:ykiho 시작할수.0부터 시작 end:ykiho 끝나는 수
	public static void HIRA_501(int totalCount, int rows) {
		String tablenm = "HIRA_501";
		int MaxPg = (int) totalCount / rows;
		int errorcnt = 0;
		int nullPg = 0;
		// DB truncate
		insertController.truncate_HIRA(tablenm);
		for(int j = 0; j<ykihoList.size(); j++){
					String url = "http://apis.data.go.kr/B551182/medicInsttDetailInfoService/getFacilityInfo?serviceKey="
							+ Config.APIKEY +"&_type=json&ykiho="+ykihoList.get(j);
					try {
						// 1. API 콜
						JSONObject item = apiController.Request_JSON_Obj(url);							
						System.out.println(j);					
						// 2. 파싱
						if (item != null) {
							List<String> result = apiController.Parse_501(item);							
							// 3. DB인서트
							insertController.insert_HIRA501(tablenm, result,ykihoList.get(j));
						}else if(item == null){	
							//오류시 한번더 콜!
							item = apiController.Request_JSON_Obj(url);							
							List<String> result = apiController.Parse_501(item);							
							// 3. DB인서트
							insertController.insert_HIRA501(tablenm, result,ykihoList.get(j));
							if(item == null){
								errorcnt++;
							}
						}
						
						if( j%1000 == 0){
							System.out.println("잠잘꺼야...");
							Thread.sleep(2000);
						}
					} catch (Exception e) {
						System.out.println("데이터가 없습니다.");
						nullPg ++;
						continue;
					}
				
		}
		System.out.println("최종 오류개수"+ykihoList.size()+ "중"+ errorcnt + "개");
		System.out.println("최종 nullpg개수"+ykihoList.size()+ "중"+ nullPg + "개");

	}
	
	
	
	//start:ykiho 시작할수.0부터 시작 end:ykiho 끝나는 수
	public static void HIRA_502(int totalCount, int rows) {
		String tablenm = "HIRA_502";
		int MaxPg = (int) totalCount / rows;
		int errorcnt = 0;
		int nullPg = 0;
		// DB truncate
		insertController.truncate_HIRA(tablenm);
		for(int j = 0; j<ykihoList.size(); j++){
					String url = "http://apis.data.go.kr/B551182/medicInsttDetailInfoService/getDetailInfo?serviceKey="
							+ Config.APIKEY +"&_type=json&ykiho="+ykihoList.get(j);
					try {
						// 1. API 콜
						JSONObject item = apiController.Request_JSON_Obj(url);							
						System.out.println(j);					
						// 2. 파싱
						if (item != null) {
							List<String> result = apiController.Parse_502(item);							
							// 3. DB인서트
							insertController.insert_HIRA502(tablenm, result,ykihoList.get(j));
						}else if(item == null){	
							//오류시 한번더 콜!
							item = apiController.Request_JSON_Obj(url);							
							List<String> result = apiController.Parse_502(item);							
							// 3. DB인서트
							insertController.insert_HIRA502(tablenm, result,ykihoList.get(j));
							if(item == null){
								errorcnt++;
							}
						}						
						if( j%1000 == 0){
							System.out.println("잠잘꺼야...");
							Thread.sleep(2000);
						}
					} catch (Exception e) {
						//System.out.println("데이터가 없습니다.");
						nullPg ++;
						continue;
					}
				
		}
		System.out.println("최종 오류개수"+ykihoList.size()+ "중"+ errorcnt + "개");
		System.out.println("최종 nullpg개수"+ykihoList.size()+ "중"+ nullPg + "개");

	}	
	
	//503은 JsonArray인 데이터도 있고 Json인 데이터도 있다.
	public static void HIRA_503(int totalCount, int rows) {
		String tablenm = "HIRA_503";
		int MaxPg = (int) totalCount / rows;
		int errorcnt = 0;
		int nullPg = 0;
		// DB truncate
		insertController.truncate_HIRA(tablenm);
		for(int j = 0; j<ykihoList.size(); j++){
					System.out.println(j);		
					String url = "http://apis.data.go.kr/B551182/medicInsttDetailInfoService/getMdlrtSbjectInfoList?serviceKey="
							+ Config.APIKEY +"&_type=json&numOfRows=" + rows+ "&ykiho="+ykihoList.get(j);
					try {	
						Object itemall = apiController.Request_JSON_ALL(url);	
						if(itemall == null){
							itemall = apiController.Request_JSON_ALL(url);
							System.out.println("한번더 해봤어연 널인가요?" + Objects.isNull(itemall) );
						}
						
						JSONArray itemlist;	
						JSONObject item;						
						if(itemall instanceof JSONArray){							
							itemlist = (JSONArray)itemall;
							if (itemlist != null) {
								List<String> result = apiController.Parse_503_Array(itemlist);		
								insertController.insert_HIRA503(tablenm, result,ykihoList.get(j));
							}
						}else if(itemall instanceof JSONObject){							
							item = (JSONObject)itemall;
							if (item != null) {
								List<String> result = apiController.Parse_503_Obj(item);							
								// 3. DB인서트
								insertController.insert_HIRA503(tablenm, result,ykihoList.get(j));
							}
						}
									
				
					} catch (Exception e) {
						System.out.println("데이터가 없습니다.");
						nullPg ++;
						continue;
					}
				
		}
		System.out.println("최종 오류개수"+ykihoList.size()+ "중"+ errorcnt + "개");
		System.out.println("최종 nullpg개수"+ykihoList.size()+ "중"+ nullPg + "개");

	}
	
	
	public static void HIRA_504(int totalCount, int rows) {
		String tablenm = "HIRA_504";
		int MaxPg = (int) totalCount / rows;
		int errorcnt = 0;
		int nullPg = 0;
		// DB truncate
		insertController.truncate_HIRA(tablenm);
		for(int j = 0; j<ykihoList.size(); j++){
					//System.out.println(j);		
					String url = "http://apis.data.go.kr/B551182/medicInsttDetailInfoService/getTransportInfoList?serviceKey="
							+ Config.APIKEY +"&_type=json&numOfRows=" + rows+ "&ykiho="+ykihoList.get(j);
					try {	
						Object itemall = apiController.Request_JSON_ALL(url);	
						if(itemall == null){
							itemall = apiController.Request_JSON_ALL(url);
							//System.out.println("한번더 해봤어연 널인가요?" + Objects.isNull(itemall) );
						}
						
						JSONArray itemlist;	
						if(itemall instanceof JSONArray){							
							itemlist = (JSONArray)itemall;
							if (itemlist != null) {
								List<String> result = apiController.Parse_504(itemlist);		
								insertController.insert_HIRA504(tablenm, result,ykihoList.get(j));
							}
						}
									
				
					} catch (Exception e) {
						//System.out.println("데이터가 없습니다.");
						nullPg ++;
						continue;
					}
				
		}
		System.out.println("최종 오류개수"+ykihoList.size()+ "중"+ errorcnt + "개");
		System.out.println("최종 nullpg개수"+ykihoList.size()+ "중"+ nullPg + "개");

	}
	
	
	public static void HIRA_505(int totalCount, int rows) {
		String tablenm = "HIRA_505";
		int MaxPg = (int) totalCount / rows;
		int errorcnt = 0;
		int nullPg = 0;
		// DB truncate
		insertController.truncate_HIRA(tablenm);
		for(int j = 0; j<ykihoList.size(); j++){
			
					//System.out.println(j);		
					String url = "http://apis.data.go.kr/B551182/medicInsttDetailInfoService/getMedicalEquipmentInfoList?serviceKey="
							+ Config.APIKEY +"&_type=json&numOfRows=" + rows+ "&ykiho="+ykihoList.get(j);
					
					try {	
						Object itemall = apiController.Request_JSON_ALL(url);	
						if(itemall == null){
							//System.out.println("오류떳긔"+url);
							itemall = apiController.Request_JSON_ALL(url);							
						}
						
						JSONArray itemlist;	
						if(itemall instanceof JSONArray){							
							itemlist = (JSONArray)itemall;
							if (itemlist != null) {
								List<String> result = apiController.Parse_505(itemlist);		
								insertController.insert_HIRA505(tablenm, result,ykihoList.get(j));
							}
						}
									
				
					} catch (Exception e) {
						//System.out.println("데이터가 없습니다.");
						nullPg ++;
						continue;
					}
				
		}
		System.out.println(tablenm);
		System.out.println("최종 오류개수"+ykihoList.size()+ "중"+ errorcnt + "개");
		System.out.println("최종 nullpg개수"+ykihoList.size()+ "중"+ nullPg + "개");
	}
	
	
	public static void HIRA_506(int totalCount, int rows) {
		String tablenm = "HIRA_506";
		int MaxPg = (int) totalCount / rows;
		int errorcnt = 0;
		int nullPg = 0;
		// DB truncate
		insertController.truncate_HIRA(tablenm);
		for(int j = 0; j<ykihoList.size(); j++){
					System.out.println(j);		
					String url = "http://apis.data.go.kr/B551182/medicInsttDetailInfoService/getCgffdAddiInfoList?serviceKey="
							+ Config.APIKEY +"&_type=json&numOfRows=" + rows+ "&ykiho="+ykihoList.get(j);
					System.out.println(url);
					try {	
						Object itemall = apiController.Request_JSON_ALL(url);	
						if(itemall == null){
							//System.out.println("오류떳긔"+url);
							itemall = apiController.Request_JSON_ALL(url);							
						}
						
						JSONArray itemlist;	
						if(itemall instanceof JSONArray){							
							itemlist = (JSONArray)itemall;
							if (itemlist != null) {
								List<String> result = apiController.Parse_506(itemlist);		
								insertController.insert_HIRA506(tablenm, result,ykihoList.get(j));
							}
						}
									
				
					} catch (Exception e) {
						//System.out.println("데이터가 없습니다.");
						nullPg ++;
						continue;
					}
				
		}
		System.out.println(tablenm);
		System.out.println("최종 오류개수"+ykihoList.size()+ "중"+ errorcnt + "개");
		System.out.println("최종 nullpg개수"+ykihoList.size()+ "중"+ nullPg + "개");

	}
	

	
	public static void HIRA_507(int totalCount, int rows) {
		String tablenm = "HIRA_507";
		int MaxPg = (int) totalCount / rows;
		int errorcnt = 0;
		int nullPg = 0;
		// DB truncate
		insertController.truncate_HIRA(tablenm);
		for(int j = 0; j<ykihoList.size(); j++){
					//System.out.println(j);		
					String url = "http://apis.data.go.kr/B551182/medicInsttDetailInfoService/getNursigGradeInfoList?serviceKey="
							+ Config.APIKEY +"&_type=json&numOfRows=" + rows+ "&ykiho="+ykihoList.get(j);
					//System.out.println(url);
					try {	
						Object itemall = apiController.Request_JSON_ALL(url);	
						if(itemall == null){
							//System.out.println("오류떳긔"+url);
							itemall = apiController.Request_JSON_ALL(url);							
						}
						
						JSONArray itemlist;	
						if(itemall instanceof JSONArray){							
							itemlist = (JSONArray)itemall;
							if (itemlist != null) {
								List<String> result = apiController.Parse_507(itemlist);		
								insertController.insert_HIRA507(tablenm, result,ykihoList.get(j));
							}
						}
									
				
					} catch (Exception e) {
						//System.out.println("데이터가 없습니다.");
						nullPg ++;
						continue;
					}
				
		}
		System.out.println(tablenm);
		System.out.println("최종 오류개수"+ykihoList.size()+ "중"+ errorcnt + "개");
		System.out.println("최종 nullpg개수"+ykihoList.size()+ "중"+ nullPg + "개");

	}
	
	
	public static void HIRA_508(int totalCount, int rows) {
		String tablenm = "HIRA_508";
		int MaxPg = (int) totalCount / rows;
		int errorcnt = 0;
		int nullPg = 0;
		// DB truncate
		insertController.truncate_HIRA(tablenm);
		for(int j = 0; j<ykihoList.size(); j++){
					//System.out.println(j);		
					String url = "http://apis.data.go.kr/B551182/medicInsttDetailInfoService/getSpclMdlrtInfoList?serviceKey="
							+ Config.APIKEY +"&_type=json&numOfRows=" + rows+ "&ykiho="+ykihoList.get(j);
					//System.out.println(url);
					try {	
						JSONArray itemall = apiController.Request_JSON(url);	
						if(itemall == null){
							//System.out.println("오류떳긔"+url);
							itemall = apiController.Request_JSON(url);							
						}
						
						JSONArray itemlist;	
						if(itemall instanceof JSONArray){							
							itemlist = (JSONArray)itemall;
							if (itemlist != null) {
								List<String> result = apiController.Parse_508(itemlist);		
								insertController.insert_HIRA508(tablenm, result,ykihoList.get(j));
							}
						}
									
				
					} catch (Exception e) {
						e.printStackTrace();
						//System.out.println("데이터가 없습니다.");
						nullPg ++;
						continue;
					}
				
		}
		System.out.println(tablenm);
		System.out.println("최종 오류개수"+ykihoList.size()+ "중"+ errorcnt + "개");
		System.out.println("최종 nullpg개수"+ykihoList.size()+ "중"+ nullPg + "개");

	}
	

	
	public static void HIRA_509(int totalCount, int rows) {
		String tablenm = "HIRA_509";
		int MaxPg = (int) totalCount / rows;
		int errorcnt = 0;
		int nullPg = 0;
		// DB truncate
		insertController.truncate_HIRA(tablenm);
		for(int j = 0; j<ykihoList.size(); j++){
					//System.out.println(j);		
					String url = "http://apis.data.go.kr/B551182/medicInsttDetailInfoService/getSpcHospAppnFieldList?serviceKey="
							+ Config.APIKEY +"&_type=json&numOfRows=" + rows+ "&ykiho="+ykihoList.get(j);
					try {	
						Object itemall = apiController.Request_JSON_ALL(url);	
						if(itemall == null){
							itemall = apiController.Request_JSON_ALL(url);
							//System.out.println("한번더 해봤어연 널인가요?" + Objects.isNull(itemall) );
						}
						
						JSONArray itemlist;	
						JSONObject item;						
						if(itemall instanceof JSONArray){							
							itemlist = (JSONArray)itemall;
							if (itemlist != null) {
								List<String> result = apiController.Parse_509_Array(itemlist);		
								insertController.insert_HIRA503(tablenm, result,ykihoList.get(j));
							}
						}else if(itemall instanceof JSONObject){							
							item = (JSONObject)itemall;
							if (item != null) {
								List<String> result = apiController.Parse_509_Obj(item);							
								// 3. DB인서트
								insertController.insert_HIRA503(tablenm, result,ykihoList.get(j));
							}
						}
									
				
					} catch (Exception e) {
						//System.out.println("데이터가 없습니다.");
						nullPg ++;
						continue;
					}
				
		}
		System.out.println(tablenm);
		System.out.println("최종 오류개수"+ykihoList.size()+ "중"+ errorcnt + "개");
		System.out.println("최종 nullpg개수"+ykihoList.size()+ "중"+ nullPg + "개");

	}
	
	
	
}