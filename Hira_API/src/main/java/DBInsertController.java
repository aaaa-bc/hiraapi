import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBInsertController {

	DBConnect dbconnect = null;
	String sql="";
	public DBInsertController(){
		dbconnect = new DBConnect();
	}

	
	
	
	
	
	public void truncate_HIRA(String tablenm){
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		sql = "truncate bepluslab_mirror."+tablenm;		
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(tablenm+"truncate 실패");
		}	
		DBClose.close(con,pstmt,rs);
	}
	
	
	public List<String> get_YkihoList(){
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		
		List<String> ykihoList = new ArrayList<>();
		sql = "select ykiho from bepluslab_mirror.HIRA_201";
		
		System.out.println(sql);
		
		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()){
				ykihoList.add(rs.getString(1));				
			}			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("YKIHOLIST 불러오기 실패");
			return ykihoList;
		}	
		DBClose.close(con,pstmt,rs);
		return ykihoList;
	}
	

	public void insert_HIRA102(String tablenm, List<String> result) { 
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){
			cols = result.get(i).split("%");
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"')";
			try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
			}			
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA103(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){
			cols = result.get(i).split("%");
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"')";
			try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
			}			
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA104(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){
			cols = result.get(i).split("%");
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"')";
			try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
			}			
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA105(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){
			cols = result.get(i).split("%");
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"')";
			try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
			}			
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA106(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){
			cols = result.get(i).split("%");
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"')";
			try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
			}			
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA201(String tablenm, List<String> result) { 
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){
			cols = result.get(i).split("%"); //,'"+cols[1]+"'		
			sql = "insert into bepluslab_mirror."+ tablenm +" values('"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"','"+cols[12]+"','"+cols[13]+"','"+cols[14]+"','"+cols[15]+"','"+cols[16]+"','"+cols[17]+"','"+cols[18]+"','"+cols[19]+"','"+cols[20]+"')";
			
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}		
		}
		DBClose.close(con,pstmt,rs);
	}

	public void insert_HIRA401(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"','"+cols[12]+"','"+cols[13]+"','"+cols[14]+"','"+cols[15]+"','"+cols[16]+"','"+cols[17]+"','"+cols[18]+"','"+cols[19]+"','"+cols[20]+"')";
			
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}		
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA402(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"','"+cols[12]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA403(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"','"+cols[12]+"','"+cols[13]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA404(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"','"+cols[12]+"','"+cols[13]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA405(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"','"+cols[12]+"','"+cols[13]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA406(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"','"+cols[12]+"','"+cols[13]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA407(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"','"+cols[12]+"','"+cols[13]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA408(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"','"+cols[12]+"','"+cols[13]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA409(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"','"+cols[12]+"','"+cols[13]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
		
	public void insert_HIRA601(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();				
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);		
	}
	
	public void insert_HIRA602(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"','"+cols[12]+"','"+cols[13]+"','"+cols[14]+"','"+cols[15]+"','"+cols[16]+"','"+cols[17]+"','"+cols[18]+"','"+cols[19]+"','"+cols[20]+"','"+cols[21]+"','"+cols[22]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();				
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
		
	public void insert_HIRA901(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"','"+cols[12]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();				
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
		
	public void insert_HIRA903(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();				
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
		
	public void insert_HIRA904(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();				
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
		
	public void insert_HIRA905(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();				
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA906(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"','"+cols[12]+"','"+cols[13]+"','"+cols[14]+"','"+cols[15]+"','"+cols[16]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();				
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA907(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"','"+cols[12]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();				
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
		
	public void insert_HIRA908(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();				
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA909(String tablenm, List<String> result) {
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){			
			cols = result.get(i).split("%"); 
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"')";
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();				
				continue;
			}	
		}
		DBClose.close(con,pstmt,rs);
	}
	
	public void insert_HIRA501(String tablenm, List<String> result, String ykiho) { 
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){
			cols = result.get(i).split("%"); //,'"+cols[1]+"'		
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+ykiho+"','"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"','"+cols[12]+"','"+cols[13]+"','"+cols[14]+"','"+cols[15]+"','"+cols[16]+"','"+cols[17]+"','"+cols[18]+"','"+cols[19]+"','"+cols[20]+"','"+cols[21]+"','"+cols[22]+"')";
			//System.out.println(sql);
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}		
		}
		DBClose.close(con,pstmt,rs);
	}
	
	
	public void insert_HIRA502(String tablenm, List<String> result, String ykiho) { 
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){
			cols = result.get(i).split("%"); //,'"+cols[1]+"'		
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+ykiho+"','"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"','"+cols[6]+"','"+cols[7]+"','"+cols[8]+"','"+cols[9]+"','"+cols[10]+"','"+cols[11]+"','"+cols[12]+"','"+cols[13]+"','"+cols[14]+"','"+cols[15]+"','"+cols[16]+"','"+cols[17]+"','"+cols[18]+"','"+cols[19]+"','"+cols[20]+"','"+cols[21]+"','"+cols[22]+"','"+cols[23]+"','"+cols[24]+"','"+cols[25]+"','"+cols[26]+"','"+cols[27]+"','"+cols[28]+"')";
			//System.out.println(sql);
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}		
		}
		DBClose.close(con,pstmt,rs);
	}
	
	
	public void insert_HIRA503(String tablenm, List<String> result, String ykiho) { 
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){
			cols = result.get(i).split("%"); //,'"+cols[1]+"'		
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+ykiho+"','"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"')";
			//System.out.println(sql);
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}		
		}
		DBClose.close(con,pstmt,rs);
	}
	
	
	
	public void insert_HIRA504(String tablenm, List<String> result, String ykiho) { 
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){
			cols = result.get(i).split("%"); //,'"+cols[1]+"'		
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+ykiho+"','"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"','"+cols[5]+"')";
			//System.out.println(sql);
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}		
		}
		DBClose.close(con,pstmt,rs);
	}
	
	
	
	public void insert_HIRA505(String tablenm, List<String> result, String ykiho) { 
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){
			cols = result.get(i).split("%"); //,'"+cols[1]+"'		
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+ykiho+"','"+cols[0]+"','"+cols[1]+"','"+cols[2]+"')";
			//System.out.println(sql);
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}		
		}
		DBClose.close(con,pstmt,rs);
	}
	
	
	public void insert_HIRA506(String tablenm, List<String> result, String ykiho) { 
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){
			cols = result.get(i).split("%"); //,'"+cols[1]+"'		
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+ykiho+"','"+cols[0]+"','"+cols[1]+"','"+cols[2]+"','"+cols[3]+"','"+cols[4]+"')";
			//System.out.println(sql);
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}		
		}
		DBClose.close(con,pstmt,rs);
	}
	
	
	public void insert_HIRA507(String tablenm, List<String> result, String ykiho) { 
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){
			cols = result.get(i).split("%"); //,'"+cols[1]+"'		
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+ykiho+"','"+cols[0]+"','"+cols[1]+"','"+cols[2]+"')";
			//System.out.println(sql);
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}		
		}
		DBClose.close(con,pstmt,rs);
	}
	
	
	public void insert_HIRA508(String tablenm, List<String> result, String ykiho) { 
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){
			cols = result.get(i).split("%"); //,'"+cols[1]+"'		
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+ykiho+"','"+cols[0]+"','"+cols[1]+"')";
			//System.out.println(sql);
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}		
		}
		DBClose.close(con,pstmt,rs);
	}
	
	
	public void insert_HIRA509(String tablenm, List<String> result, String ykiho) { 
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		String sql = "";
		String[] cols;		
		for (int i=0;i<result.size();i++){
			cols = result.get(i).split("%"); //,'"+cols[1]+"'		
			sql = "insert into bepluslab_mirror."+ tablenm +" values(null,'"+ykiho+"','"+cols[0]+"','"+cols[1]+"')";
			//System.out.println(sql);
		try {
				pstmt = con.prepareStatement(sql);
				pstmt.execute();
			} catch (SQLException e) {
				System.out.println("DB오류");
				e.printStackTrace();
				continue;
			}		
		}
		DBClose.close(con,pstmt,rs);
	}
	
	
}
