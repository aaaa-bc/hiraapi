import java.sql.Connection; 
import java.sql.DriverManager;

public class DBConnect {
	public DBConnect() {}
	
	public static Connection getConnection() {
		String url = Config.URL;
		String id = Config.ID;
		String pass = Config.PASS;
		
		Connection con = null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(url,id,pass);
		}catch(Exception e) {
			System.out.println(e);
		}
		return con;
	}
}


