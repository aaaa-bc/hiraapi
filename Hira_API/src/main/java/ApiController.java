
/*
 * 2017-11-20 Made by YJ
 * 
 * */
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.simple.parser.JSONParser;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class ApiController {

	// API를 호출. 공통
	public JSONArray Request_JSON(String url) {
		HttpResponse<JsonNode> response = null;
		JSONObject rawobj = null;
		JSONArray itemlist = null;
		try {		
			//System.out.println(url);			
			response = Unirest.get(url).asJson();			
			rawobj = response.getBody().getObject();
			itemlist = rawobj.getJSONObject("response").getJSONObject("body").getJSONObject("items")
					.getJSONArray("item");	
		} catch (UnirestException e) {
			System.out.println("API 오류");
			return null;
		}
		return itemlist;
	}
	
	
	public JSONObject Request_JSON_Obj(String url) {
		HttpResponse<JsonNode> response = null;
		JSONObject rawobj = null;
		JSONObject item = null;
		JSONArray itemlist = null;
		try {		
			System.out.println(url);			
			response = Unirest.get(url).asJson();			
			rawobj = response.getBody().getObject();	
			item = rawobj.getJSONObject("response").getJSONObject("body").getJSONObject("items")
					.getJSONObject("item");
		
		} catch (UnirestException e) {
			e.printStackTrace();
			System.out.println("오류url "+ url);
			return null;
		}
		return item;
	}
	
	
	public Object Request_JSON_ALL(String url) {
		HttpResponse<JsonNode> response = null;
		JSONObject rawobj = null;
		Object item = null;
		try {		
			//System.out.println(url);			
			response = Unirest.get(url).asJson();			
			rawobj = response.getBody().getObject();	
			item = rawobj.getJSONObject("response").getJSONObject("body").getJSONObject("items")
					.get("item");
		
		} catch (UnirestException e) {
			e.printStackTrace();
			System.out.println("오류url "+ url);
			return null;
		}
		return item;
	}
	
	
	
	

	// 서비스:병원코드정보서비스
	// 오퍼레이션(영문):getMedicInsttClassesCodeList
	// 오퍼레이션(국문):의료기관종별코드조회
	public List<String> Parse_102(JSONArray itemlist) {
		System.out.println("parse102");
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[2];
			temp[0] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			temp[1] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			result.add(temp[0] + '%' + temp[1]);
		}
		return result;
	}

	// 서비스:병원코드정보서비스
	// 오퍼레이션(영문):getMdlrtSbjectCodeList
	// 오퍼레이션(국문):진료과목코드조회
	public List<String> Parse_103(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[3];
			temp[0] = (String) itemlist.getJSONObject(i).get("dgsbjtCd").toString().trim();
			temp[1] = (String) itemlist.getJSONObject(i).get("dgsbjtCdCmmt").toString().trim();
			temp[2] = (String) itemlist.getJSONObject(i).get("dgsbjtCdNm").toString().trim();
			result.add(temp[0] + '%' + temp[1] + '%' + temp[2]);
		}
		return result;
	}

	// 서비스:병원코드정보서비스
	// 오퍼레이션(영문):getMedicEquipmentCodeList
	// 오퍼레이션(국문):장비코드조회
	public List<String> Parse_104(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[2];
			temp[0] = (String) itemlist.getJSONObject(i).get("oftCd").toString().trim();
			temp[1] = (String) itemlist.getJSONObject(i).get("oftCdNm").toString().trim();
			result.add(temp[0] + '%' + temp[1]);
		}
		return result;
	}

	// 서비스:병원코드정보서비스
	// 오퍼레이션(영문):getSpclMdlrtCodeList
	// 오퍼레이션(국문):장비코드조회
	public List<String> Parse_105(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[3];
			temp[0] = (String) itemlist.getJSONObject(i).get("srchCd").toString().trim();
			temp[1] = (String) itemlist.getJSONObject(i).get("srchCdNm").toString().trim();
			temp[2] = (String) itemlist.getJSONObject(i).get("srchCdCmmt").toString().trim();
			result.add(temp[0] + '%' + temp[1] + '%' + temp[2]);
		}
		return result;
	}

	// 서비스:병원코드정보서비스
	// 오퍼레이션(영문):getSpcHospCodeList
	// 오퍼레이션(국문):전문병원코드조회
	public List<String> Parse_106(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[3];
			temp[0] = (String) itemlist.getJSONObject(i).get("srchCd").toString().trim();
			temp[1] = (String) itemlist.getJSONObject(i).get("srchCdNm").toString().trim();
			temp[2] = (String) itemlist.getJSONObject(i).get("srchCdCmmt").toString().trim();
			result.add(temp[0] + '%' + temp[1] + '%' + temp[2]);
		}

		return result;
	}

	// 서비스:병원정보서비스
	// 오퍼레이션(영문):getHospBasisList
	// 오퍼레이션(국문):병원기본목록
	public List<String> Parse_201(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[21];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("postNo").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("telno").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}

			try {
				temp[12] = (String) itemlist.getJSONObject(i).get("hospUrl").toString().trim();
			} catch (Exception e) {
				temp[12] = "";
			}

			try {
				temp[13] = (String) itemlist.getJSONObject(i).get("estbDd").toString().trim();
			} catch (Exception e) {
				temp[13] = "";
			}

			try {
				temp[14] = (String) itemlist.getJSONObject(i).get("drTotCnt").toString().trim();
			} catch (Exception e) {
				temp[14] = "";
			}

			try {
				temp[15] = (String) itemlist.getJSONObject(i).get("gdrCnt").toString().trim();
			} catch (Exception e) {
				temp[15] = "";
			}

			try {
				temp[16] = (String) itemlist.getJSONObject(i).get("intnCnt").toString().trim();
			} catch (Exception e) {
				temp[16] = "";
			}

			try {
				temp[17] = (String) itemlist.getJSONObject(i).get("resdntCnt").toString().trim();
			} catch (Exception e) {
				temp[17] = "";
			}

			try {
				temp[18] = (String) itemlist.getJSONObject(i).get("sdrCnt").toString().trim();
			} catch (Exception e) {
				temp[18] = "";
			}

			try {
				temp[19] = (String) itemlist.getJSONObject(i).get("XPos").toString().trim();
			} catch (Exception e) {
				temp[19] = "0.0";
			}

			try {
				temp[20] = (String) itemlist.getJSONObject(i).get("YPos").toString().trim();
			} catch (Exception e) {
				temp[20] = "0.0";
			}

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11] + '%'
					+ temp[12] + '%' + temp[13] + '%' + temp[14] + '%' + temp[15] + '%' + temp[16] + '%' + temp[17]
					+ '%' + temp[18] + '%' + temp[19] + '%' + temp[20]);
		}
		return result;
	}

	// 서비스:getSpclMdlrtHospList
	// 오퍼레이션(영문):getSpclMdlrtHospList
	// 오퍼레이션(국문):특수진료병원목록
	public List<String> Parse_401(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[21];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("postNo").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("telno").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}

			try {
				temp[12] = (String) itemlist.getJSONObject(i).get("hospUrl").toString().trim();
			} catch (Exception e) {
				temp[12] = "";
			}

			try {
				temp[13] = (String) itemlist.getJSONObject(i).get("estbDd").toString().trim();
			} catch (Exception e) {
				temp[13] = "";
			}

			try {
				temp[14] = (String) itemlist.getJSONObject(i).get("drTotCnt").toString().trim();
			} catch (Exception e) {
				temp[14] = "";
			}

			try {
				temp[15] = (String) itemlist.getJSONObject(i).get("gdrCnt").toString().trim();
			} catch (Exception e) {
				temp[15] = "";
			}

			try {
				temp[16] = (String) itemlist.getJSONObject(i).get("intnCnt").toString().trim();
			} catch (Exception e) {
				temp[16] = "";
			}

			try {
				temp[17] = (String) itemlist.getJSONObject(i).get("resdntCnt").toString().trim();
			} catch (Exception e) {
				temp[17] = "";
			}

			try {
				temp[18] = (String) itemlist.getJSONObject(i).get("sdrCnt").toString().trim();
			} catch (Exception e) {
				temp[18] = "";
			}

			try {
				temp[19] = (String) itemlist.getJSONObject(i).get("XPos").toString().trim();
			} catch (Exception e) {
				temp[19] = "0.0";
				// temp[19] = " ";
			}

			try {
				temp[20] = (String) itemlist.getJSONObject(i).get("YPos").toString().trim();
			} catch (Exception e) {
				temp[20] = "0.0";
				// temp[20] = " ";
			}

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11] + '%'
					+ temp[12] + '%' + temp[13] + '%' + temp[14] + '%' + temp[15] + '%' + temp[16] + '%' + temp[17]
					+ '%' + temp[18] + '%' + temp[19] + '%' + temp[20]);
		}
		return result;
	}

	// 서비스:특수진료병원정보서비스
	// 오퍼레이션(영문):getSpcHospList
	// 오퍼레이션(국문):진료과목
	public List<String> Parse_402(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[13];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("postNo").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("telno").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}

			try {
				temp[12] = (String) itemlist.getJSONObject(i).get("hospUrl").toString().trim();
			} catch (Exception e) {
				temp[12] = " ";
			}

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11] + '%'
					+ temp[12]);
		}
		return result;
	}

	// 서비스:특수진료병원정보서비스
	// 오퍼레이션(영문):getMdlrtSbjectList
	// 오퍼레이션(국문):전문병원
	public List<String> Parse_403(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[14];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("postNo").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("telno").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}

			try {
				temp[12] = (String) itemlist.getJSONObject(i).get("hospUrl").toString().trim();
			} catch (Exception e) {
				temp[12] = "";
			}

			try {
				temp[13] = (String) itemlist.getJSONObject(i).get("estbDd").toString().trim();
			} catch (Exception e) {
				temp[13] = " ";
			}

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11] + '%'
					+ temp[12] + '%' + temp[13]);
		}
		return result;
	}

	// 서비스:특수진료병원정보서비스
	// 오퍼레이션(영문):getRcperHospList
	// 오퍼레이션(국문):요양병원
	public List<String> Parse_404(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[14];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("postNo").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("telno").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}

			try {
				temp[12] = (String) itemlist.getJSONObject(i).get("hospUrl").toString().trim();
			} catch (Exception e) {
				temp[12] = " ";
			}

			try {
				temp[13] = (String) itemlist.getJSONObject(i).get("estbDd").toString().trim();
			} catch (Exception e) {
				temp[13] = " ";
			}

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11] + '%'
					+ temp[12] + '%' + temp[13]);
		}
		return result;
	}

	// 서비스:특수진료병원정보서비스
	// 오퍼레이션(영문):getDrgList
	// 오퍼레이션(국문):포괄수가
	public List<String> Parse_405(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[14];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("postNo").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("telno").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}

			try {
				temp[12] = (String) itemlist.getJSONObject(i).get("hospUrl").toString().trim();
			} catch (Exception e) {
				temp[12] = " ";
			}

			try {
				temp[13] = (String) itemlist.getJSONObject(i).get("estbDd").toString().trim();
			} catch (Exception e) {
				temp[13] = " ";
			}

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11] + '%'
					+ temp[12] + '%' + temp[13]);
		}
		return result;
	}
	
	public List<String> Parse_406(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[14];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("postNo").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("telno").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}

			try {
				temp[12] = (String) itemlist.getJSONObject(i).get("hospUrl").toString().trim();
			} catch (Exception e) {
				temp[12] = " ";
			}

			try {
				temp[13] = (String) itemlist.getJSONObject(i).get("estbDd").toString().trim();
			} catch (Exception e) {
				temp[13] = " ";
			}

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11] + '%'
					+ temp[12] + '%' + temp[13]);
		}
		return result;
	}
	
	public List<String> Parse_407(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[14];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("postNo").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("telno").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}

			try {
				temp[12] = (String) itemlist.getJSONObject(i).get("hospUrl").toString().trim();
			} catch (Exception e) {
				temp[12] = " ";
			}

			try {
				temp[13] = (String) itemlist.getJSONObject(i).get("estbDd").toString().trim();
			} catch (Exception e) {
				temp[13] = " ";
			}

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11] + '%'
					+ temp[12] + '%' + temp[13]);
		}
		return result;
	}

	public List<String> Parse_408(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[14];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("postNo").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("telno").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}

			try {
				temp[12] = (String) itemlist.getJSONObject(i).get("hospUrl").toString().trim();
			} catch (Exception e) {
				temp[12] = " ";
			}

			try {
				temp[13] = (String) itemlist.getJSONObject(i).get("estbDd").toString().trim();
			} catch (Exception e) {
				temp[13] = " ";
			}

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11] + '%'
					+ temp[12] + '%' + temp[13]);
		}
		return result;
	}

	public List<String> Parse_409(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[14];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("postNo").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("telno").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}

			try {
				temp[12] = (String) itemlist.getJSONObject(i).get("hospUrl").toString().trim();
			} catch (Exception e) {
				temp[12] = " ";
			}

			try {
				temp[13] = (String) itemlist.getJSONObject(i).get("estbDd").toString().trim();
			} catch (Exception e) {
				temp[13] = " ";
			}

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11] + '%'
					+ temp[12] + '%' + temp[13]);
		}
		return result;
	}

	public List<String> Parse_601(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[9];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("divCd1").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("divCd1Nm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("divCd1Dsc").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("divCd2").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("divCd2Nm").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("divCd2Dsc").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("divCd3").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("divCd3Nm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("divCd3Dsc").toString().trim();
			} catch (Exception e) {
				temp[8] = " ";
			}

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8]);
		}
		return result;
	}

	public List<String> Parse_602(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[23];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("url").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("divCd1").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("divCd1Nm").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("divCd2").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}

			try {
				temp[12] = (String) itemlist.getJSONObject(i).get("divCd2Nm").toString().trim();
			} catch (Exception e) {
				temp[12] = "";
			}

			try {
				temp[13] = (String) itemlist.getJSONObject(i).get("divCd3").toString().trim();
			} catch (Exception e) {
				temp[13] = "";
			}

			try {
				temp[13] = (String) itemlist.getJSONObject(i).get("divCd3").toString().trim();
			} catch (Exception e) {
				temp[13] = "";
			}

			try {
				temp[14] = (String) itemlist.getJSONObject(i).get("divCd3Nm").toString().trim();
			} catch (Exception e) {
				temp[14] = "";
			}

			try {
				temp[15] = (String) itemlist.getJSONObject(i).get("prcMin").toString().trim();
			} catch (Exception e) {
				temp[15] = "";
			}

			try {
				temp[16] = (String) itemlist.getJSONObject(i).get("prcMax").toString().trim();
			} catch (Exception e) {
				temp[16] = "";
			}

			try {
				temp[17] = (String) itemlist.getJSONObject(i).get("itmCd").toString().trim();
			} catch (Exception e) {
				temp[17] = "";
			}

			try {
				temp[18] = (String) itemlist.getJSONObject(i).get("itmCdNm").toString().trim();
			} catch (Exception e) {
				temp[18] = "";
			}

			try {
				temp[19] = (String) itemlist.getJSONObject(i).get("itmPrcMin").toString().trim();
			} catch (Exception e) {
				temp[19] = "";
			}

			try {
				temp[20] = (String) itemlist.getJSONObject(i).get("itmPrcMax").toString().trim();
			} catch (Exception e) {
				temp[20] = "";
			}

			try {
				temp[21] = (String) itemlist.getJSONObject(i).get("invtDt").toString().trim();
			} catch (Exception e) {
				temp[21] = "";
			}

			try {
				temp[22] = (String) itemlist.getJSONObject(i).get("rmk1").toString().trim();
			} catch (Exception e) {
				temp[22] = " ";
			}

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11] + '%'
					+ temp[12] + '%' + temp[13] + '%' + temp[14] + '%' + temp[15] + '%' + temp[16] + '%' + temp[17]
					+ '%' + temp[18] + '%' + temp[19] + '%' + temp[20] + '%' + temp[21] + '%' + temp[22]);
		}
		return result;
	}
		
	public List<String> Parse_901(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[13];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("asmDivNm").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("asmExsCrtrtxt").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("asmGrdCmmtTxt").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("asmItmCd").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("asmItmCdNm").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("asmRstGdTxt").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("grd1CrtrTxt").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("grd2CrtrTxt").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("grd3CrtrTxt").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("grd4CrtrTxt").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("grd5CrtrTxt").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("grdExsCrtrTxt").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}

			try {
				temp[12] = (String) itemlist.getJSONObject(i).get("grdnCrtrTxt").toString().trim();
			} catch (Exception e) {
				temp[12] = " ";
			}

		

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11] + '%'
					+ temp[12]);
		}
		return result;
	}
		
	public List<String> Parse_903(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[12];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("asmCzitmCd").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("asmGrd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("asmItmCd").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}

	

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11]);
		}
		return result;
	}
			
	public List<String> Parse_904(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[12];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("asmCzitmCd").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("asmGrd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[5] = " ";
			}
			
			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}

	

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%' + temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10]+ '%' + temp[11]);
		}
		return result;
	}
		
	public List<String> Parse_905(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[12];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("asmCzitmCd").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("asmGrd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}

	

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11]);
		}
		return result;
	}
		
	public List<String> Parse_906(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[17];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("asmGrd1").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("asmGrd2").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("asmGrd3").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("asmGrd4").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("asmGrd5").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("asmGrd6").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("asmItmCd").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}
			
			try {
				temp[12] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[12] = "";
			}
			
			try {
				temp[13] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[13] = "";
			}
			
			try {				
				temp[14] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[14] = "";
			}
			
			try {
				temp[15] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[15] = "";
			}
			
			try {
				temp[16] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[16] = "";
			}
				

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11]
					 + '%' + temp[12] + '%' + temp[13] + '%' + temp[14] + '%' + temp[15] + '%' + temp[16]);
		
		}
		return result;
	}

	public List<String> Parse_907(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[13];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("asmGrd1").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("asmGrd2").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("asmItmCd").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}

			try {
				temp[12] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[12] = " ";
			}
		

			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11] + '%'
					+ temp[12]);
		}
		return result;
	}

	public List<String> Parse_908(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[12];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("asmGrd").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("asmItmCd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}


			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11]);
		}
		return result;
	}

	public List<String> Parse_909(JSONArray itemlist) { 
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[12];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("addr").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("asmGrd").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("asmItmCd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("clCd").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) itemlist.getJSONObject(i).get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) itemlist.getJSONObject(i).get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) itemlist.getJSONObject(i).get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) itemlist.getJSONObject(i).get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) itemlist.getJSONObject(i).get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) itemlist.getJSONObject(i).get("ykiho").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}


			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11]);
		}
		return result;
	}


	public List<String> Parse_501(JSONObject item) {  
		List<String> result = new ArrayList<String>();
	
			String temp[] = new String[23];

			try {
				temp[0] = (String) item.get("yadmNm").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) item.get("clCd").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) item.get("clCdNm").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) item.get("orgTyCd").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) item.get("orgTyCdNm").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) item.get("sidoCd").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) item.get("sidoCdNm").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) item.get("sgguCd").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) item.get("sgguCdNm").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) item.get("emdongNm").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) item.get("postNo").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) item.get("addr").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}
			
			try {
				temp[12] = (String) item.get("telno").toString().trim();
			} catch (Exception e) {
				temp[12] = "";
			}
			
			try {
				temp[13] = (String) item.get("hghrSickbdCnt").toString().trim();
			} catch (Exception e) {
				temp[13] = "";
			}
			
			try {
				temp[14] = (String) item.get("stdSickbdCnt").toString().trim();
			} catch (Exception e) {
				temp[14] = "";
			}
			
			try {
				temp[15] = (String) item.get("aduChldSprmCnt").toString().trim();
			} catch (Exception e) {
				temp[15] = "";
			}
			
			try {
				temp[16] = (String) item.get("nbySprmCnt").toString().trim();
			} catch (Exception e) {
				temp[16] = "";
			}
			
			try {
				temp[17] = (String) item.get("partumCnt").toString().trim();
			} catch (Exception e) {
				temp[17] = "";
			}
			
			try {
				temp[18] = (String) item.get("soprmCnt").toString().trim();
			} catch (Exception e) {
				temp[18] = "";
			}
			
			try {
				temp[19] = (String) item.get("emymCnt").toString().trim();
			} catch (Exception e) {
				temp[19] = "";
			}
			
			try {
				temp[20] = (String) item.get("ptrmCnt").toString().trim();
			} catch (Exception e) {
				temp[20] = "";
			}
			
			try {
				temp[21] = (String) item.get("hospUrl").toString().trim();
			} catch (Exception e) {
				temp[21] = "";
			}
			
			try {
				temp[22] = (String) item.get("estbDd").toString().trim();
			} catch (Exception e) {
				temp[22] = " ";
			}
			


			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11] + '%' + temp[12]
					+ '%' + temp[13] + '%' + temp[14] + '%' + temp[15] + '%' + temp[16] + '%' + temp[17] + '%' + temp[18]
					+ '%' + temp[19] + '%' + temp[20] + '%' + temp[21] + '%' + temp[22]);
			
		
		return result;
	}

	public List<String> Parse_502(JSONObject item) {  
		List<String> result = new ArrayList<String>();
	
			String temp[] = new String[29];

			try {
				temp[0] = (String) item.get("plcNm").toString().trim().replace("'","");
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) item.get("plcDir").toString().trim().replace("'","");
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) item.get("plcDist").toString().trim().replace("'","");
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) item.get("parkQty").toString().trim();
			} catch (Exception e) {
				temp[3] = "0";
			}

			try {
				temp[4] = (String) item.get("parkXpnsYn").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) item.get("parkEtc").toString().trim();
			} catch (Exception e) {
				temp[5] = "";
			}

			try {
				temp[6] = (String) item.get("noTrmtSun").toString().trim();
			} catch (Exception e) {
				temp[6] = "";
			}

			try {
				temp[7] = (String) item.get("noTrmtHoli").toString().trim();
			} catch (Exception e) {
				temp[7] = "";
			}

			try {
				temp[8] = (String) item.get("emyDayYn").toString().trim();
			} catch (Exception e) {
				temp[8] = "";
			}

			try {
				temp[9] = (String) item.get("emyDayTelNo1").toString().trim();
			} catch (Exception e) {
				temp[9] = "";
			}

			try {
				temp[10] = (String) item.get("emyDayTelNo2").toString().trim();
			} catch (Exception e) {
				temp[10] = "";
			}

			try {
				temp[11] = (String) item.get("emyNgtYn").toString().trim();
			} catch (Exception e) {
				temp[11] = "";
			}
			
			try {
				temp[12] = (String) item.get("emyNgtTelNo1").toString().trim();
			} catch (Exception e) {
				temp[12] = "";
			}
			
			try {
				temp[13] = (String) item.get("emyNgtTelNo2").toString().trim();
			} catch (Exception e) {
				temp[13] = "";
			}
			
			try {
				temp[14] = (String) item.get("lunchWeek").toString().trim();
			} catch (Exception e) {
				temp[14] = "";
			}
			
			try {
				temp[15] = (String) item.get("rcvSat").toString().trim();
			} catch (Exception e) {
				temp[15] = "";
			}
			
			try {
				temp[16] = (String) item.get("rcvWeek").toString().trim();
			} catch (Exception e) {
				temp[16] = "";
			}
			
			try {
				temp[17] = (String) item.get("trmtMonStart").toString().trim();
			} catch (Exception e) {
				temp[17] = "";
			}
			
			try {
				temp[18] = (String) item.get("trmtMonEnd").toString().trim();
			} catch (Exception e) {
				temp[18] = "";
			}
			
			try {
				temp[19] = (String) item.get("trmtTueStart").toString().trim();
			} catch (Exception e) {
				temp[19] = "";
			}
			
			try {
				temp[20] = (String) item.get("trmtTueEnd").toString().trim();
			} catch (Exception e) {
				temp[20] = "";
			}
			
			try {
				temp[21] = (String) item.get("trmtWedStart").toString().trim();
			} catch (Exception e) {
				temp[21] = "";
			}
			
			try {
				temp[22] = (String) item.get("trmtWedEnd").toString().trim();
			} catch (Exception e) {
				temp[22] = " ";
			}
			
			try {
				temp[23] = (String) item.get("trmtThuStart").toString().trim();
			} catch (Exception e) {
				temp[23] = " ";
			}
			
			
			try {
				temp[24] = (String) item.get("trmtThuEnd").toString().trim();
			} catch (Exception e) {
				temp[24] = " ";
			}
			
			
			try {
				temp[25] = (String) item.get("trmtFriStart").toString().trim();
			} catch (Exception e) {
				temp[25] = " ";
			}
			
			
			try {
				temp[26] = (String) item.get("trmtFriEnd").toString().trim();
			} catch (Exception e) {
				temp[26] = " ";
			}
			
			
			try {
				temp[27] = (String) item.get("trmtSatStart").toString().trim();
			} catch (Exception e) {
				temp[27] = " ";
			}
			
			
			try {
				temp[28] = (String) item.get("trmtSatEnd").toString().trim();
			} catch (Exception e) {
				temp[28] = " ";
			}
			


			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5] + '%'
					+ temp[6] + '%' + temp[7] + '%' + temp[8] + '%' + temp[9] + '%' + temp[10] + '%' + temp[11] + '%' + temp[12]
					+ '%' + temp[13] + '%' + temp[14] + '%' + temp[15] + '%' + temp[16] + '%' + temp[17] + '%' + temp[18]
					+ '%' + temp[19] + '%' + temp[20] + '%' + temp[21] + '%' + temp[22] + '%' + temp[23] + '%' + temp[24]
					+ '%' + temp[25] + '%' + temp[26] + '%' + temp[27] + '%' + temp[28]);
			
		
		return result;
	}

	
	public List<String> Parse_503_Obj(JSONObject item) { 
		List<String> result = new ArrayList<String>();
			String temp[] = new String[4];

			try {
				temp[0] = (String) item.get("dgsbjtCd").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) item.get("dgsbjtCdNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) item.get("dgsbjtPrSdrCnt").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) item.get("cdiagDrCnt").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}
	
			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3]);
		
		return result;
	}
	
	
	public List<String> Parse_503_Array(JSONArray itemlist) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[4];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("dgsbjtCd").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("dgsbjtCdNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("dgsbjtPrSdrCnt").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("cdiagDrCnt").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}
	
			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3]);
		}
		return result;
	}
	
	public List<String> Parse_504(JSONArray itemlist){
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[6];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("trafNm").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("lineNo").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("arivPlc").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("dir").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("dist").toString().trim();
			} catch (Exception e) {
				temp[4] = "";
			}

			try {
				temp[5] = (String) itemlist.getJSONObject(i).get("rmk").toString().trim();
			} catch (Exception e) {
				temp[5] = " ";
			}
			
			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4] + '%' + temp[5]);
		}
		return result;
	}
	
	
	public List<String> Parse_505(JSONArray itemlist){
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[3];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("oftCd").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("oftCdNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("oftCnt").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}
			
			result.add(temp[0] + '%' + temp[1] + '%' + temp[2]);
		}
		return result;
	}
	
	
	public List<String> Parse_506(JSONArray itemlist){
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[5];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("tyCd").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("tyCdNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("gnmAddcYn").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			try {
				temp[3] = (String) itemlist.getJSONObject(i).get("calcNopCnt").toString().trim();
			} catch (Exception e) {
				temp[3] = "";
			}

			try {
				temp[4] = (String) itemlist.getJSONObject(i).get("trmealGrd").toString().trim();
			} catch (Exception e) {
				temp[4] = "0";
			}

			
			result.add(temp[0] + '%' + temp[1] + '%' + temp[2] + '%' + temp[3] + '%' + temp[4]);
		}
		return result;
	}
	
	
	public List<String> Parse_507(JSONArray itemlist){
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[2];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("srchCd").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("srchCdNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}

			try {
				temp[2] = (String) itemlist.getJSONObject(i).get("careGrd").toString().trim();
			} catch (Exception e) {
				temp[2] = "";
			}

			
			result.add(temp[0] + '%' + temp[1] + '%' + temp[2]);
		}
		return result;
	}
	
	public List<String> Parse_508(JSONArray itemlist){
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[2];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("srchCd").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("srchCdNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}


			
			result.add(temp[0] + '%' + temp[1]);
		}
		return result;
	}
	
	
	public List<String> Parse_509_Array(JSONArray itemlist){
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < itemlist.length(); i++) {
			String temp[] = new String[2];

			try {
				temp[0] = (String) itemlist.getJSONObject(i).get("srchCd").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) itemlist.getJSONObject(i).get("srchCdNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}


			
			result.add(temp[0] + '%' + temp[1]);
		}
		return result;
	}
	
	
	public List<String> Parse_509_Obj(JSONObject item){
		List<String> result = new ArrayList<String>();
		
			String temp[] = new String[2];
			try {
				temp[0] = (String) item.get("srchCd").toString().trim();
			} catch (Exception e) {
				temp[0] = "";
			}

			try {
				temp[1] = (String) item.get("srchCdNm").toString().trim();
			} catch (Exception e) {
				temp[1] = "";
			}
			
			result.add(temp[0] + '%' + temp[1]);
		
		return result;
	}
	
	
	
}
