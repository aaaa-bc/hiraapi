import org.json.JSONObject; 

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.core.IsNull;
import org.json.JSONArray;
import org.json.simple.parser.JSONParser;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class MainController {

	static public int totalCount = 100000;
	static public int numOfRows = 1000;
	static ApiController apiController = new ApiController();
	static DBInsertController insertController = new DBInsertController();
	static DetailController detailController = new DetailController();

	public static void main(String[] args) throws Exception {
/*사용법:
10X,20X,40X,60X,906~909-->HIRA_ALL
901~905까지는 HIRA_ALL이지만 개별function 추출
50X --> detailController.HIRA_501 사용.

*/
		//HIRA_ALL("HIRA_908", totalCount,numOfRows); 
		
		//detailController.HIRA_901(totalCount, numOfRows);
		//detailController.HIRA_501(totalCount, numOfRows);
		//detailController.HIRA_505(totalCount, numOfRows);
		//detailController.HIRA_506(totalCount, numOfRows);
		detailController.HIRA_507(totalCount, numOfRows);
		//detailController.HIRA_507(totalCount, numOfRows);
		//detailController.HIRA_508(totalCount, numOfRows);
		//detailController.HIRA_509(totalCount, numOfRows);
		
		//detailController.HIRA_502(totalCount, numOfRows);
		
	}

	// 병원코드정보서비스, 병원정보서비스, 특수진료병원정보서비스, 비급여진료비정보서비스, 병원평가결과정보조회서비스 
	public static void HIRA_ALL(String tablenm, int totalCount, int rows) {
		int MaxPg = (int) totalCount / rows;
		insertController.truncate_HIRA(tablenm);
		String Service = "";
		String operation = "";
		
		//url(서비스 및 오퍼레이션)을 정의한다.
		switch (tablenm) {
			/* 병원코드정보서비스 */
			case "HIRA_102":
				Service = "codeInfoService";
				operation = "getMedicInsttClassesCodeList";
				break;
			case "HIRA_103":
				Service = "codeInfoService";
				operation = "getMdlrtSbjectCodeList";
				break;
			case "HIRA_104":
				Service = "codeInfoService";
				operation = "getMedicEquipmentCodeList";
				break;
			case "HIRA_105":
				Service = "codeInfoService";
				operation = "getSpclMdlrtCodeList";
				break;
			case "HIRA_106":
				Service = "codeInfoService";
				operation = "getSpcHospCodeList";
				break;
	
				
			/* 병원정보서비스 */
			case "HIRA_201":
				Service = "hospInfoService";
				operation = "getHospBasisList";
				break;
	
				
			/* 특수진료병원정보서비스 */
			case "HIRA_401":
				Service = "spclMdlrtHospInfoService";
				operation = "getSpclMdlrtHospList";
				break;
			case "HIRA_402":
				Service = "spclMdlrtHospInfoService";
				operation = "getMdlrtSbjectList";
				break;
			case "HIRA_403":
				Service = "spclMdlrtHospInfoService";
				operation = "getSpcHospList";
				break;
			case "HIRA_404":
				Service = "spclMdlrtHospInfoService";
				operation = "getRcperHospList";
				break;
			case "HIRA_405":
				Service = "spclMdlrtHospInfoService";
				operation = "getDrgList";
				break;
			case "HIRA_406":
				Service = "spclMdlrtHospInfoService";
				operation = "getSurgeryList";
				break;
			case "HIRA_407":
				Service = "spclMdlrtHospInfoService";
				operation = "getInorgTrnspntList";
				break;
			case "HIRA_408":
				Service = "spclMdlrtHospInfoService";
				operation = "getChildNightMdlrtList";
				break;
			case "HIRA_409":
				Service = "spclMdlrtHospInfoService";
				operation = "getSpcifyFieldList";
				break;
	
				
			/* 비급여진료비정보서비스 */
			case "HIRA_601":
				Service = "nonPaymentDamtInfoService";
				operation = "getNonPaymentItemCodeList";
				break;
			case "HIRA_602":
				Service = "nonPaymentDamtInfoService";
				operation = "getNonPaymentItemHospList";
				break;
	
				
			/* 병원평가결과정보조회서비스 */
			case "HIRA_901":
				Service = "hospAsmRstInfoService";
				operation = "getHospAsmGrdCrtrList";
				HIRA_901(totalCount, rows);
				break;
			case "HIRA_902":
				Service = "hospAsmRstInfoService";
				operation = "getHospWhlAsmRstList";
				HIRA_902(totalCount, rows);
				break;
			case "HIRA_903":
				Service = "hospAsmRstInfoService";
				operation = "getDissAsmRstList";
				HIRA_903(totalCount, rows);
				break;
			case "HIRA_904":
				Service = "hospAsmRstInfoService";
				operation = "getSoprAsmRstList";
				HIRA_904(totalCount, rows);
				break;
			case "HIRA_905":
				Service = "hospAsmRstInfoService";
				operation = "getPrscAsmRstList";
				HIRA_905(totalCount, rows);
				break;
			case "HIRA_906":
				Service = "hospAsmRstInfoService";
				operation = "getSoprDiagQtyAsmRstList";
				break;
			case "HIRA_907":
				Service = "hospAsmRstInfoService";
				operation = "getCnrSoprDeathAsmRstList";
				break;
			case "HIRA_908":
				Service = "hospAsmRstInfoService";
				operation = "getRcperHospAsmRstList";
				break;
			case "HIRA_909":
				Service = "hospAsmRstInfoService";
				operation = "getGnhpSprmAsmRstList";
				break;
	
			default:
				break;
		}
		
		//url을 만들고, API를 파싱, DB에 정보를 insert한다.
		if(tablenm != "HIRA_901" || tablenm !="HIRA_902" || tablenm !="HIRA_903" || tablenm != "HIRA_904" || tablenm != "HIRA_905"){
			for (int i = 1; i < MaxPg; i++) {
				String url = "http://apis.data.go.kr/B551182/" + Service + "/" + operation + "?serviceKey=" + Config.APIKEY
						+ "&pageNo=" + i + "&numOfRows=" + numOfRows + "&_type=json";				
				try {
					// 1. API 콜
					JSONArray itemlist = apiController.Request_JSON(url);
					System.out.println(MaxPg + "중" + i + "페이지");
					if (itemlist != null) {
						List<String> result;
						switch (tablenm) {
						/* 병원코드정보서비스 */
						case "HIRA_102":
							result = apiController.Parse_102(itemlist);
							insertController.insert_HIRA102(tablenm, result);
							break;
						case "HIRA_103":
							result = apiController.Parse_103(itemlist);
							insertController.insert_HIRA103(tablenm, result);
							break;
						case "HIRA_104":
							result = apiController.Parse_104(itemlist);
							insertController.insert_HIRA104(tablenm, result);
							break;
						case "HIRA_105":
							result = apiController.Parse_105(itemlist);
							insertController.insert_HIRA105(tablenm, result);
							break;
						case "HIRA_106":
							result = apiController.Parse_106(itemlist);
							insertController.insert_HIRA106(tablenm, result);
							break;
	
						/* 병원정보서비스 */
						case "HIRA_201":
							result = apiController.Parse_201(itemlist);
							insertController.insert_HIRA201(tablenm, result);
							break;
	
						/* 특수진료병원정보서비스 */
						case "HIRA_401":
							result = apiController.Parse_401(itemlist);
							insertController.insert_HIRA401(tablenm, result);
							break;
						case "HIRA_402":
							result = apiController.Parse_402(itemlist);
							insertController.insert_HIRA402(tablenm, result);
							break;
						case "HIRA_403":
							result = apiController.Parse_403(itemlist);
							insertController.insert_HIRA403(tablenm, result);
							break;
						case "HIRA_404":
							result = apiController.Parse_404(itemlist);
							insertController.insert_HIRA404(tablenm, result);
							break;
						case "HIRA_405":
							result = apiController.Parse_405(itemlist);
							insertController.insert_HIRA405(tablenm, result);
							break;
						case "HIRA_406":
							result = apiController.Parse_406(itemlist);
							insertController.insert_HIRA406(tablenm, result);
							break;
						case "HIRA_407":
							result = apiController.Parse_407(itemlist);
							insertController.insert_HIRA407(tablenm, result);
							break;
						case "HIRA_408":
							result = apiController.Parse_408(itemlist);
							insertController.insert_HIRA408(tablenm, result);
							break;
						case "HIRA_409":
							result = apiController.Parse_409(itemlist);
							insertController.insert_HIRA409(tablenm, result);
							break;
	
						/* 비급여진료비정보서비스 */
						case "HIRA_601":
							result = apiController.Parse_601(itemlist);
							insertController.insert_HIRA601(tablenm, result);
							break;
						case "HIRA_602":
							result = apiController.Parse_602(itemlist);
							insertController.insert_HIRA602(tablenm, result);
							break;
							
						/* 병원평가결과정보조회서비스 */
						case "HIRA_906":
							result = apiController.Parse_906(itemlist);
							insertController.insert_HIRA906(tablenm, result);
							break;
						case "HIRA_907":
							result = apiController.Parse_907(itemlist);
							insertController.insert_HIRA907(tablenm, result);
							break;
						case "HIRA_908":
							result = apiController.Parse_908(itemlist);
							insertController.insert_HIRA908(tablenm, result);
							break;
						case "HIRA_909":
							result = apiController.Parse_909(itemlist);
							insertController.insert_HIRA909(tablenm, result);
							break;
	
						default:
							break;
	
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					break;
				}
			}
		}
	}

	public static void HIRA_901(int totalCount, int rows) {
		String tablenm = "HIRA_901";
		int MaxPg = (int) totalCount / rows;
		// DB truncate
		insertController.truncate_HIRA(tablenm);

		for (int i = 1; i < MaxPg; i++) {
			String url = "http://apis.data.go.kr/B551182/hospAsmRstInfoService/getHospAsmGrdCrtrList?serviceKey="
					+ Config.APIKEY + "&pageNo=" + i + "&numOfRows=" + numOfRows + "&_type=json&asmItmCd=00";
			try {
				// 1. API 콜
				JSONArray itemlist = apiController.Request_JSON(url);
				// 2. 파싱
				if (itemlist != null) {
					List<String> result = apiController.Parse_901(itemlist);
					// 3. DB인서트
					insertController.insert_HIRA901(tablenm, result);
				}
			} catch (Exception e) {
				System.out.println("데이터가 없습니다.");
				break;
			}
		}

	}

	public static void HIRA_902(int totalCount, int rows) {
		String tablenm = "HIRA_902";
		int MaxPg = (int) totalCount / rows;
		// DB truncate
		insertController.truncate_HIRA(tablenm);

		for (int i = 1; i < MaxPg; i++) {
			String url = "http://apis.data.go.kr/B551182/hospAsmRstInfoService/getHospWhlAsmRstList?serviceKey="
					+ Config.APIKEY + "&pageNo=" + i + "&numOfRows=" + numOfRows + "&_type=json&asmItmCd=00";
			try {
				// 1. API 콜
				JSONArray itemlist = apiController.Request_JSON(url);
				// 2. 파싱
				if (itemlist != null) {
					List<String> result = apiController.Parse_901(itemlist);
					// 3. DB인서트
					insertController.insert_HIRA901(tablenm, result);
				}
			} catch (Exception e) {
				System.out.println("데이터가 없습니다.");
				break;
			}
		}
	}

	public static void HIRA_903(int totalCount, int rows) {
		String tablenm = "HIRA_903";
		int MaxPg = (int) totalCount / rows;
		// DB truncate
		insertController.truncate_HIRA(tablenm);
		String[] asmItmCd = { "01", "02", "03", "04", "05", "16", "22", "23", "25" };

		for (int i = 1; i < MaxPg; i++) {
			for (int j = 0; j < asmItmCd.length; j++) {
				String url = "http://apis.data.go.kr/B551182/hospAsmRstInfoService/getDissAsmRstList?serviceKey="
						+ Config.APIKEY + "&pageNo=" + i + "&numOfRows=" + numOfRows + "&_type=json&asmItmCd="
						+ asmItmCd[j];
				try {
					// 1. API 콜
					JSONArray itemlist = apiController.Request_JSON(url);
					// 2. 파싱
					if (itemlist != null) {
						List<String> result = apiController.Parse_903(itemlist);
						// 3. DB인서트
						insertController.insert_HIRA903(tablenm, result);
					}
				} catch (Exception e) {
					System.out.println("데이터가 없습니다.");
					continue;
				}
			}
			if (i == 10) {
				break;
			}
		}
	}

	public static void HIRA_904(int totalCount, int rows) {
		String tablenm = "HIRA_904";
		int MaxPg = (int) totalCount / rows;
		// DB truncate
		insertController.truncate_HIRA(tablenm);
		String[] asmItmCd = { "06", "08", "09", "17", "19", "21" };
		for (int i = 1; i < MaxPg; i++) {
			for (int j = 0; j < asmItmCd.length; j++) {
				String url = "http://apis.data.go.kr/B551182/hospAsmRstInfoService/getSoprAsmRstList?serviceKey="
						+ Config.APIKEY + "&pageNo=" + i + "&numOfRows=" + numOfRows + "&_type=json&asmItmCd="
						+ asmItmCd[j];
				try {
					// 1. API 콜
					JSONArray itemlist = apiController.Request_JSON(url);
					// 2. 파싱
					if (itemlist != null) {
						List<String> result = apiController.Parse_904(itemlist);
						// 3. DB인서트
						insertController.insert_HIRA904(tablenm, result);
					}
				} catch (Exception e) {
					System.out.println("데이터가 없습니다.");
					continue;
				}
			}
			if (i == 10) {
				break;
			}
		}
	}

	public static void HIRA_905(int totalCount, int rows) {
		String tablenm = "HIRA_905";
		int MaxPg = (int) totalCount / rows;
		// DB truncate
		insertController.truncate_HIRA(tablenm);
		String[] asmItmCd = { "11", "12", "13", "15", "18" };

		for (int i = 1; i < MaxPg; i++) {
			for (int j = 0; j < asmItmCd.length; j++) {
				String url = "http://apis.data.go.kr/B551182/hospAsmRstInfoService/getPrscAsmRstList?serviceKey="
						+ Config.APIKEY + "&pageNo=" + i + "&numOfRows=" + numOfRows + "&_type=json&asmItmCd="
						+ asmItmCd[j];
				try {
					// 1. API 콜
					JSONArray itemlist = apiController.Request_JSON(url);
					// 2. 파싱
					if (itemlist != null) {
						List<String> result = apiController.Parse_905(itemlist);
						// 3. DB인서트
						insertController.insert_HIRA905(tablenm, result);
					}
				} catch (Exception e) {
					System.out.println("데이터가 없습니다.");
					continue;
				}
			}

		}
	}
	
	
	

	// 병원평가결과정보조회서비스
	// HIRA901,902,903,904,905 는 기존폼과 달라서, 따로 function을 빼줌. HIRA906,907,908,909
	// 는 공통
/*	public static void HIRA_90X(String tablenm, int totalCount, int rows) {
		int MaxPg = (int) totalCount / rows;
		insertController.truncate_HIRA(tablenm);
		String Service = "";
		String operation = "";
		switch (tablenm) {
		 병원코드정보서비스 
		case "HIRA_901":
			Service = "hospAsmRstInfoService";
			operation = "getHospAsmGrdCrtrList";
			HIRA_901(totalCount, rows);
			break;
		case "HIRA_902":
			Service = "hospAsmRstInfoService";
			operation = "getHospWhlAsmRstList";
			HIRA_902(totalCount, rows);
			break;
		case "HIRA_903":
			Service = "hospAsmRstInfoService";
			operation = "getDissAsmRstList";
			HIRA_903(totalCount, rows);
			break;
		case "HIRA_904":
			Service = "hospAsmRstInfoService";
			operation = "getSoprAsmRstList";
			HIRA_904(totalCount, rows);
			break;
		case "HIRA_905":
			Service = "hospAsmRstInfoService";
			operation = "getPrscAsmRstList";
			HIRA_905(totalCount, rows);
			break;
		case "HIRA_906":
			Service = "hospAsmRstInfoService";
			operation = "getSoprDiagQtyAsmRstList";
			break;
		case "HIRA_907":
			Service = "hospAsmRstInfoService";
			operation = "getCnrSoprDeathAsmRstList";
			break;
		case "HIRA_908":
			Service = "hospAsmRstInfoService";
			operation = "getRcperHospAsmRstList";
			break;
		case "HIRA_909":
			Service = "hospAsmRstInfoService";
			operation = "getGnhpSprmAsmRstList";
			break;
		default:
			break;
		}

		if (tablenm == "HIRA_906" || tablenm == "HIRA_907" || tablenm == "HIRA_908" || tablenm == "HIRA_909") {
			for (int i = 1; i < MaxPg; i++) {
				String url = "http://apis.data.go.kr/B551182/" + Service + "/" + operation + "?serviceKey="
						+ Config.APIKEY + "&pageNo=" + i + "&numOfRows=" + numOfRows + "&_type=json";
				try {
					// 1. API 콜
					JSONArray itemlist = apiController.Request_JSON(url);
					System.out.println(MaxPg + "중" + i + "페이지");
					if (itemlist != null) {
						List<String> result;
						switch (tablenm) {
						case "HIRA_906":
							result = apiController.Parse_906(itemlist);
							insertController.insert_HIRA906(tablenm, result);
							break;
						case "HIRA_907":
							result = apiController.Parse_907(itemlist);
							insertController.insert_HIRA907(tablenm, result);
							break;
						case "HIRA_908":
							result = apiController.Parse_908(itemlist);
							insertController.insert_HIRA908(tablenm, result);
						case "HIRA_909":
							result = apiController.Parse_909(itemlist);
							insertController.insert_HIRA909(tablenm, result);

							break;
						default:
							break;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					break;
				}
			}
		}
	}
*/

}